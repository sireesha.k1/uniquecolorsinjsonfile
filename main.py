import json



def unique_colors(data):
    if not isinstance(data, str):
        for k, v in data.items():
            if isinstance(v, dict):
                unique_colors(v)
            elif hasattr(v, '__iter__') and not isinstance(v, str):
                for dictitem in v:
                    unique_colors(dictitem)
            elif isinstance(v, str):
                if k == "color":
                    lstColors.append(v)
            else:
                if k == "color":
                    lstColors.append(v)
    return list(set(lstColors))


jsonfile = open('scripts/core/jsonData.json', 'r')
data = json.load(jsonfile)
lstColors = []
print(type(data))
print(unique_colors(data))
